from rdflib import Graph, Namespace, RDF, RDFS, OWL, URIRef, DCTERMS, Literal, XSD

# Definition des NameNSraums
NS = Namespace("http://osm.hpi.de/ontology/fta#")

VERSION = "0.0.1"

# Erstellung des Graphen
g = Graph()
g.namespace_manager.bind("fta", NS)

# Ontology
ontology = URIRef("https://gitlab.hpi.de/osm/flexidug/models/fault-tree-analysis")
g.add((ontology, RDF.type, OWL.Ontology))
g.add((ontology, DCTERMS.title, Literal("Fault Tree Analysis Ontology")))
g.add((ontology, DCTERMS.description, Literal("A rdf Ontology for Fault Tree Analysis definition.",lang='en')))

g.add((ontology, OWL.versionInfo, Literal(VERSION)))
g.add((ontology, DCTERMS.creator, Literal("Dirk Friedenberger")))
g.add((ontology, DCTERMS.creator, URIRef("https://www.researchgate.net/profile/Dirk_Friedenberger")))

g.add((ontology, DCTERMS.identifier, Literal(NS)))

# Classes
g.add((NS.Event, RDF.type, OWL.Class))
g.add((NS.Event, RDFS.comment, Literal("An abstract event in fault tree.", lang='en')))

g.add((NS.Gate, RDF.type, OWL.Class))
g.add((NS.Gate, RDFS.comment, Literal("An abstract gate in fault tree.", lang='en')))

g.add((NS.IntermediateEvent, RDF.type, OWL.Class))
g.add((NS.IntermediateEvent, RDFS.comment, Literal("An intermediate event in fault tree.", lang='en')))

g.add((NS.BasicEvent, RDF.type, OWL.Class))
g.add((NS.BasicEvent, RDFS.comment, Literal("An basic event in fault tree.", lang='en')))

g.add((NS.TopEvent, RDF.type, OWL.Class))
g.add((NS.TopEvent, RDFS.comment, Literal("The top event in fault tree.", lang='en')))

g.add((NS.OrGate, RDF.type, OWL.Class))
g.add((NS.OrGate, RDFS.comment, Literal("A OR gate in fault tree.", lang='en')))

g.add((NS.VotingOrGate, RDF.type, OWL.Class))
g.add((NS.VotingOrGate, RDFS.comment, Literal("A Voting OR gate in fault tree.", lang='en')))

g.add((NS.AndGate, RDF.type, OWL.Class))
g.add((NS.AndGate, RDFS.comment, Literal("A AND gate in fault tree.", lang='en')))


# Festlegung der Oberklasse für die Klassen
g.add((NS.IntermediateEvent, RDFS.subClassOf, NS.Event))
g.add((NS.BasicEvent, RDFS.subClassOf, NS.Event))
g.add((NS.TopEvent, RDFS.subClassOf, NS.IntermediateEvent))

g.add((NS.OrGate, RDFS.subClassOf, NS.Gate))
g.add((NS.VotingOrGate, RDFS.subClassOf, NS.Gate))
g.add((NS.AndGate, RDFS.subClassOf, NS.Gate))


# Hinzufügen der Attribute
g.add((NS.probability, RDF.type, OWL.DatatypeProperty))
g.add((NS.probability, RDFS.domain, NS.BasicEvent))
g.add((NS.probability, RDFS.range, XSD.float))

g.add((NS.threshold, RDF.type, OWL.DatatypeProperty))
g.add((NS.threshold, RDFS.domain, NS.VotingOrGate))
g.add((NS.threshold, RDFS.range, XSD.integer))

# Hinzufügen der Relationen
g.add((NS.hasGate, RDF.type, OWL.ObjectProperty))
g.add((NS.hasGate, RDFS.domain, NS.Event))
g.add((NS.hasGate, RDFS.range, NS.Gate))

g.add((NS.hasEvent, RDF.type, OWL.ObjectProperty))
g.add((NS.hasEvent, RDFS.domain, NS.Gate))
g.add((NS.hasEvent, RDFS.range, NS.Event))


# Serialisierung der Ontologie in RDF/XML-Syntax
g.serialize(destination=f"fault-tree-analysis-{VERSION}.ttl",format='turtle')
