# Fault Tree Analysis

Fault Tree Analysis is a rdf Ontology for Fault Tree Analysis definition.
## Installation

if needed the ttl file could be recrated.
```bash
python create_ontology.py
```

## Usage

Visualize with https://service.tib.eu/webvowl/

![Asset Ontology](./fault-tree-analysis-0.0.1.ttl.svg)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)